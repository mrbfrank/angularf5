var modules = [
    // 3rd party
    'ui.router',
    'templates'

    // components
    // inject component modules here...
];

angular.module('Wilder', modules);

function appConfig(
    $stateProvider,
    $urlRouterProvider
) {

    // For any unmatched url, redirect to index
    $urlRouterProvider.otherwise('index');

    $stateProvider
        .state('index', {
            url: "",
            views: {
                'topnav': {
                    templateUrl: 'layout/topnav.html'
                },
                'body': {
                    templateUrl: 'layout/body.html'
                },
                'footer': {
                    templateUrl: 'layout/footer.html'
                }
            }
        });


}

function appRun (
    $rootScope
) {
    // Initialize Foundation
    $rootScope.$on('$viewContentLoaded', function () {
        $(document).foundation();
    });
}

angular
    .module('Wilder')
    .config(appConfig)
    .run(appRun);
