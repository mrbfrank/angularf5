var gulp = require('gulp');
var templateCache = require('gulp-angular-templatecache');
var concat = require('gulp-concat');
var connect = require('gulp-connect');
var minifyCSS = require('gulp-minify-css');
var ngAnnotate = require('gulp-ng-annotate');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var buffer = require('vinyl-buffer');

var paths = {
    sass: [
        './app/scss/app.scss'
    ],
    js: ['./app/js/**/*.js'],
    html: ['./app/index.html'],
    templates: ['./app/js/**/*.html']
};

var libs = [
    // AngularJS
    './bower_components/angular/angular.js',
    './bower_components/angular-ui-router/release/angular-ui-router.js',

    // Foundation5 dependencies
    './bower_components/jquery/dist/jquery.js',
    './bower_components/fastclick/lib/fastclick.js',

    // Foundation5
    './bower_components/foundation/js/foundation.js',

    // Foundation5 components, uncomment as needed
    './bower_components/foundation/js/foundation/foundation.abide.js',
    './bower_components/foundation/js/foundation/foundation.accordion.js',
    './bower_components/foundation/js/foundation/foundation.alert.js',
    './bower_components/foundation/js/foundation/foundation.clearing.js',
    './bower_components/foundation/js/foundation/foundation.dropdown.js',
    './bower_components/foundation/js/foundation/foundation.equalizer.js',
    './bower_components/foundation/js/foundation/foundation.interchange.js',
    './bower_components/foundation/js/foundation/foundation.joyride.js',
    './bower_components/foundation/js/foundation/foundation.js',
    './bower_components/foundation/js/foundation/foundation.magellan.js',
    './bower_components/foundation/js/foundation/foundation.offcanvas.js',
    './bower_components/foundation/js/foundation/foundation.orbit.js',
    './bower_components/foundation/js/foundation/foundation.reveal.js',
    './bower_components/foundation/js/foundation/foundation.slider.js',
    './bower_components/foundation/js/foundation/foundation.tab.js',
    './bower_components/foundation/js/foundation/foundation.tooltip.js',
    './bower_components/foundation/js/foundation/foundation.topbar.js'

];

gulp.task('connect', function() {
    connect.server({
        livereload: true,
        root: 'build/',
        port: 8085,
        host: "0.0.0.0"
    });
});

gulp.task('sass', function() {
    return gulp.src(paths.sass)
        .pipe(sass())
        .pipe(concat('app.css'))
        .pipe(buffer())
        .pipe(minifyCSS({keepBreaks: false}))
        .pipe(gulp.dest('./build/css'))
        .pipe(connect.reload());
});

gulp.task('js', function() {
    return gulp.src(paths.js)
        .pipe(ngAnnotate())
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(concat('app.js'))
        .pipe(buffer())
        // .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./build/'))
        .pipe(connect.reload());
});

gulp.task('libs', function() {
    return gulp.src(libs)
        .pipe(concat('libs.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./build/'))
        .pipe(connect.reload());
});

gulp.task('build-index', function() {
    gulp.src('./app/index.html')
        .pipe(gulp.dest('./build/'))
        .pipe(connect.reload());
});

gulp.task('build-templates', function() {
    gulp.src('./app/js/**/*.html')
        .pipe(templateCache({standalone: true}))
        .pipe(gulp.dest('./build'))
        .pipe(connect.reload());
});

gulp.task('watch', function() {
    gulp.watch(paths.sass, ['sass']);
    gulp.watch('./app/scss/**/*', ['sass']);
    gulp.watch(paths.js, ['js']);
    gulp.watch(paths.html, ['build-index']);
    gulp.watch(paths.templates, ['build-templates']);
});

gulp.task('default', [
    'connect',
    'watch',
    'sass',
    'libs',
    'js',
    'build-index',
    'build-templates'
]);
